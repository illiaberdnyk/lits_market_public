-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: lits_cv_java_1
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Lialian',NULL,NULL,NULL),(2,'admin@gmail.com',1,'admin@gmail.com','$2a$10$8ZkuAGlHB6jCywrOGv7/CuqQHAVlfWxaqmulZzTeHw31o7kRFwWQa'),(3,'Liliana',1,'lil@gmail.com','$2a$10$yZLOaBvEo6uMLpkKx2z49uz4XIecmuSxbNn1AlWpLOliXDduySU46'),(4,'oleg',1,'ogobchuk@gmail.com','$2a$10$HJjCEbpSW/FrVzp0ffjlIuiX9vLsHbGFIQzPkrxzPwQbxd7uzIukW'),(7,'Illia',1,'illia.berdnyk@gmail.com','$2a$10$D5we4ja1fd7/vb9bwn2x0eOFPNzf.hN/87Gg/GUIkT/v/ktYSgs7a'),(8,'Lili',1,'lilisla83@gmail.com','$2a$10$NcuyfcXtvV6SEupqZJtC0.7ME2Hh85mmdDemwpqRhbboltOU/35Ry'),(9,'qwe',1,'qwe@qwe.q','$2a$10$aBHks0pJFS2zvle9LfXrcuGq68bCi0VAiZsJFYittCKucGUr2UnPm'),(10,'zxc',1,'zxc@zxc','$2a$10$/4/El8z8I1x4Dm1fz7uHmursxWVGNcf7vY.FTrugFvRCcVSX0PMoS'),(11,'asd',1,'asd@asd','$2a$10$NBWg2K6hSnLG8/537qQfjeBmLfJDrUFq409DHpkPsBv1KdbzMPqIS'),(12,'Liliana',2,'lil_admin@gmail.com','$2a$10$yZLOaBvEo6uMLpkKx2z49uz4XIecmuSxbNn1AlWpLOliXDduySU46'),(13,'Liliana',3,'lil_manager@gmail.com','$2a$10$yZLOaBvEo6uMLpkKx2z49uz4XIecmuSxbNn1AlWpLOliXDduySU46'),(14,'Liliana',4,'lil_seller@gmail.com','$2a$10$yZLOaBvEo6uMLpkKx2z49uz4XIecmuSxbNn1AlWpLOliXDduySU46');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-13 23:38:37
