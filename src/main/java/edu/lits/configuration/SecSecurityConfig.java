package edu.lits.configuration;

import edu.lits.pojo.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.thymeleaf.extras.springsecurity5.dialect.SpringSecurityDialect;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Configuration
@EnableWebSecurity
public class SecSecurityConfig
		extends WebSecurityConfigurerAdapter {

	private final UserDetailsService userDetailsService;

	public SecSecurityConfig(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/customer/**").hasRole("CUSTOMER")
				.antMatchers("/user/seller/info", "/seller/**").hasRole("SELLER")
				.antMatchers("/login*").anonymous()
				.antMatchers("/noAuth", "/main", "/", "/index",
		"/bootstrap/**", "/jquery/**", "/css/**", "/js/**", "/images/**", "/test", "/registration", "/addUser", "/greeting").permitAll().anyRequest()
				.authenticated()
				.and()
				.formLogin()
				.loginPage("/login")
				.permitAll()
				.successHandler(new SuccessRequestHandler())

				.successForwardUrl("/index")
				.and()
				.logout()
				.permitAll()
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
				.logoutSuccessUrl("/index");
	//.and().csrf().disable();
	}

	class SuccessRequestHandler extends SavedRequestAwareAuthenticationSuccessHandler {

        @Override
        protected String determineTargetUrl(HttpServletRequest request, HttpServletResponse response) {

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			Role role = ((SecurityUser) auth.getPrincipal()).getRole();
            String targetUrl = "";
			switch (role.getName()) {
				case "CUSTOMER":
					targetUrl = "/index";
					break;
				case "ADMIN":
					targetUrl = "/admin";
					break;
				case "MANAGER":
					targetUrl = "/manager/profile/info";
					break;
				case "SELLER":
					targetUrl = "/user/seller/info";
					break;
				default:
					throw new IllegalArgumentException(String.format("Specify target url for new role - %s", role.getName()));
			}
            return targetUrl;
        }
    }

	@Bean
	public SpringSecurityDialect springSecurityDialect() {
		return new SpringSecurityDialect();
	}

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

	@Bean(name="multipartResolver")
	public CommonsMultipartResolver getResolver(){
		CommonsMultipartResolver resolver = new CommonsMultipartResolver();
		//Set the maximum allowed size (in bytes) for each individual file.
		resolver.setMaxUploadSizePerFile(5242880);//5MB
		return resolver;
	}
}
