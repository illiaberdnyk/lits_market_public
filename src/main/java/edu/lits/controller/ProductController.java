package edu.lits.controller;

import edu.lits.dal.CartDal;
import edu.lits.dal.ProductDal;
import edu.lits.model.ProductModel;
import edu.lits.pojo.Cart;
import edu.lits.pojo.Order;
import edu.lits.pojo.Product;
import edu.lits.service.ProductService;
import edu.lits.service.StorageService;
import edu.lits.service.common.CurrentUserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class ProductController {

    @Autowired
    private StorageService storageService;

    @Autowired
    private CartDal cartDal;
    @Autowired
    private ProductService productService;

    public void setCartDal(CartDal cartDal) {
        this.cartDal = cartDal;
    }

    @RequestMapping(value = "/product",
            method = RequestMethod.GET)
    public String productView(Model model) {

        Product product = new Product();
        product.setId(1);
        product.setName("product1");
        product.setPrice(111);


        model.addAttribute("product", product);
        return "product";
    }

    @RequestMapping(value = "/getOrderList",
            method = RequestMethod.GET)
    public String orderView(Model model) {


        return "orderList";
    }

    @RequestMapping(value = "/add/product/to/cart",
            method = RequestMethod.POST)
    public String addProductToCart(Model model,
                                   @ModelAttribute ProductModel productModel) {


        Product product = new Product();

        Cart cart = new Cart();
        Order order = new Order();
        order.setId(1);

        cart.setProduct(product);
        cart.setNumberOfProducts(5);

        cartDal.saveOrUpdate(cart);


        model.addAttribute("product", product);
        return "cart";
    }

    @RequestMapping(value = "/products",method = RequestMethod.GET)
    public String productList(Model model){
        List<Product> products= productService.getProductList();
        model.addAttribute("products",products);

        return "/products/products";
    }

    @RequestMapping(value ="/products/new",method = RequestMethod.GET )
    public String getProductPage(Model model){
        ProductModel productModel = new ProductModel();
        model.addAttribute("editProduct", productModel);
        return "products/newProduct";
    }

    @RequestMapping(value="/products",method = RequestMethod.POST)
    public String create(@Validated @ModelAttribute ProductModel newProductModel){
        productService.create(newProductModel);
        return "redirect:/products";
    }

    @RequestMapping(value = "/products/{id}", method = RequestMethod.GET)
    public String getEditPage(@PathVariable Integer id, Model model) {
        Product product = productService.read(id);
        ProductModel modelOfProduct = new ProductModel();
        modelOfProduct.setId(product.getId());
        modelOfProduct.setName(product.getName());
        modelOfProduct.setPrice(product.getPrice());
        modelOfProduct.setDescription(product.getDescription());
        modelOfProduct.setExtension(product.getImageExtension());
        model.addAttribute("editProduct",modelOfProduct);
        return "/products/newProduct";
    }

    @ResponseBody
    @RequestMapping(value = "/products/{id}/image", method = RequestMethod.GET)
    public ResponseEntity<Resource> getImage(@PathVariable Integer id) {
        Resource resource = productService.loadImage(id);
        String fileName = resource == null ? null : resource.getFilename();
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + fileName + "\"").body(resource);
    }

    @RequestMapping(value ="/products/{id}",method = RequestMethod.POST )
    public String edit(@PathVariable Integer id, @ModelAttribute ProductModel editedProductModel, Model model) {
        productService.edit(editedProductModel, id);
        return "redirect:/products";
    }

    @PreAuthorize("hasRole('SELLER')")
    @RequestMapping(value="/products/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable Integer id){
        productService.delete(id);
        return "redirect:/products";
    }

    @RequestMapping(value="/products/{id}/image", method = RequestMethod.DELETE)
    public String deleteImage(@PathVariable Integer id){
        productService.deleteImage(id);
        return "redirect:/products";
    }
}
