package edu.lits.dal;

import edu.lits.pojo.Cart;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

@Component
public class CartDalImpl extends CrudOperationsImpl<Cart> implements CartDal<Cart> {

    private SessionFactory sessionFactory;

    public CartDalImpl(SessionFactory sessionFactory) {
        super(Cart.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
