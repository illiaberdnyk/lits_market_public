package edu.lits.dal;

import edu.lits.pojo.Order;
import edu.lits.pojo.OrderToProduct;
import edu.lits.pojo.Product;

import java.util.List;

public interface OrderToProductDal extends CrudOperations<OrderToProduct> {

    OrderToProduct getByOrderAndProduct(Order currentOrder, Product product);

    List<OrderToProduct> getByOrderId(Integer orderId);

}
