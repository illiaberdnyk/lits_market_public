package edu.lits.dal;

import edu.lits.pojo.Order;
import edu.lits.pojo.OrderToProduct;
import edu.lits.pojo.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderToProductDalImpl extends CrudOperationsImpl<OrderToProduct> implements OrderToProductDal {

    private SessionFactory sessionFactory;

    public OrderToProductDalImpl(SessionFactory sessionFactory) {
        super(OrderToProduct.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public OrderToProduct getByOrderAndProduct(Order currentOrder, Product product) {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        OrderToProduct orderToProduct = (OrderToProduct) session.createQuery("select otp from edu.lits.pojo.OrderToProduct otp" +
                " where otp.order = :order and " +
                "otp.product = :product")
                .setParameter("order", currentOrder)
                .setParameter("product", product)
                .uniqueResult();
        tx.commit();
        session.close();
        return orderToProduct;
    }

    @Override
    public List<OrderToProduct> getByOrderId(Integer orderId) {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        List<OrderToProduct> orderToProduct = (List<OrderToProduct>) session.createQuery("select otp from edu.lits.pojo.OrderToProduct otp" +
                "  join fetch otp.product where otp.order.id = :orderId")
                .setParameter("orderId", orderId)
                .list();
        tx.commit();
        session.close();
        return orderToProduct;

    }


}
