package edu.lits.dal;

import edu.lits.pojo.User;

import java.util.List;

public interface UserDal extends CrudOperations<User> {

    List<User> userList();

    User getByEmail(String email);

    User findByMinOrders();

    List<User> findAllCustomers();
}
