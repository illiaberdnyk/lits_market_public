package edu.lits.service;

import edu.lits.pojo.Cart;

import java.util.List;

public interface CartService {

    public List<Cart> getCartList();

}
