package edu.lits.service;

import edu.lits.dal.OrderToProductDal;
import edu.lits.pojo.Order;
import edu.lits.pojo.OrderToProduct;
import edu.lits.pojo.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderToProductServiceImpl implements OrderToProductService {

    @Autowired
    private OrderToProductDal orderToProductDal;

    @Override
    public OrderToProduct saveOrUpdate(OrderToProduct orderToProduct) {
        return orderToProductDal.saveOrUpdate(orderToProduct);
    }

    @Override
    public OrderToProduct getByOrderAndProduct(Order currentOrder, Product product) {
        return orderToProductDal.getByOrderAndProduct(currentOrder, product);
    }

    @Override
    public void delete(OrderToProduct orderToProduct) {
        orderToProductDal.delete(orderToProduct);

    }

    @Override
    public List<OrderToProduct> getByOrderId(Integer orderId) {

        return orderToProductDal.getByOrderId(orderId);
    }


}
