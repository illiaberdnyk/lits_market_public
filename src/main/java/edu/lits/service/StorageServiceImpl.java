package edu.lits.service;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Service
public class StorageServiceImpl implements StorageService {

    @Value("${image.location}")
    private String imageLocation;

    @Override
    public void store(MultipartFile image, String location) {
        try {
            String extension = FilenameUtils.getExtension(image.getOriginalFilename());

            image.transferTo(new File(imageLocation + location + "." + extension));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Resource load(String path) {
        return new FileSystemResource(imageLocation + path);
    }

    @Override
    public boolean delete(String imagePath) {
        File file = new File(imageLocation + imagePath);
        return file.delete();
    }
}
