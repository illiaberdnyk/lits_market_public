package edu.lits.service;

import edu.lits.dal.CrudOperations;
import edu.lits.dal.CrudOperationsImpl;
import edu.lits.model.UserModel;
import edu.lits.pojo.User;
import org.springframework.core.io.Resource;

import java.util.List;

public interface UserService extends CrudOperations<User> {

    User saveOrUpdate (User user);
    User save (UserModel userModel);
    User read (Integer id);
    void delete (User user);

    User getByEmail(String email);
    List<User> findAllCustomers();
    void saveImage(UserModel userModel, User user);

    Resource loadImage(Integer id);
}
