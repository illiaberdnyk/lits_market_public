package edu.lits.service.common;

import edu.lits.configuration.SecurityUser;

public interface CurrentUserService {

    SecurityUser securityUser();

}

