package edu.lits.validation;

import edu.lits.pojo.User;
import edu.lits.service.UserService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueEmailValidator implements ConstraintValidator<UniqueEmail, String> {

   private final UserService userService;

   public UniqueEmailValidator(UserService userService) {
      this.userService = userService;
   }

   public void initialize(UniqueEmail constraint) {
   }

   public boolean isValid(String obj, ConstraintValidatorContext context) {

      User user = userService.getByEmail(obj);
      return user == null;
   }
}
