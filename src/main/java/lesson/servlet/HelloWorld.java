package lesson.servlet;


import lesson.Group;
import lesson.MainJDBC;

import java.io.*;
import java.util.List;
import javax.servlet.*;
import javax.servlet.http.*;


// Extend HttpServlet class
public class HelloWorld extends HttpServlet {

	private String message;

	public void init() throws ServletException {
		// Do required initialization
		message = "Hello Servlet";
	}

	public void doGet(HttpServletRequest request,
					  HttpServletResponse response)
			throws ServletException, IOException {

		response.getWriter().write("Hello servlet");

	}

	public void doPost(HttpServletRequest request,
					   HttpServletResponse response)
			throws ServletException, IOException {

		response.getWriter().write("Hello servlet");
	}

	void redirect(HttpServletRequest request,
				  HttpServletResponse response) {
		RequestDispatcher requestDispatcher =
				request.getRequestDispatcher("anotherPage.html");
		try {
			requestDispatcher.forward(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void destroy() {
		// do nothing.
	}
}