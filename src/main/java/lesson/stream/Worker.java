package lesson.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Worker {

	private List<Book> listBook = new ArrayList<>();

	public void addBooks(List<Book> bookListValue) {
		this.listBook.addAll(bookListValue);
	}

	public Worker() {
		listBook.add(new Book("",1D,1));
	}

	public List<Book> getListBook() {
		return listBook;
	}

	public void setListBook(List<Book> listBook) {
		this.listBook = listBook;
	}

	public static void main(String[] args) {
		Book book1 = new Book("ield1", 23D, 3);
		Book book2 = new Book("ield2", 23D, 23);
		Book book3 = new Book("iel3", 23D, 13);
		Book book4 = new Book("Field4", 23D, 2);
		Book book5 = new Book("Fiel5", 23D, 123);
		Book book6 = new Book("Field7", 23D, 123);

		List<Book> bookList = new ArrayList<>();

		bookList.add(book1);
		bookList.add(book2);
		bookList.add(book3);
		bookList.add(book4);
		bookList.add(book4);
		bookList.add(book5);
		bookList.add(book6);
		bookList.add(book6);
		bookList.add(book1);

		for (Book item : bookList) {
			System.out.println(item.getFieldS());
		}


		List<Book> books = bookList.stream()
				.filter(x -> x.getFieldI() > 10)
				.filter(q -> q.getFieldS().startsWith("F"))
				.collect(Collectors.toList());
		System.out.println(123);

		Book testBook;
		int q = 0;
		for (int i = 0; i < 1_000_000; i++) {
			q++;
			if (q % 10 == 0) {
				Runtime.getRuntime().gc();

			}
			System.out.println(i);
			testBook = new Book("a", 1d, i);
		}

		for (Book item : books) {
			System.out.println(item.getFieldS());
		}

	}
}
